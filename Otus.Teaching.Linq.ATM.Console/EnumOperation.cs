﻿namespace Otus.Teaching.Linq.ATM
{
    /// <summary>
    /// Перечисление операций банкомата
    /// </summary>
    public enum OperationATM
    {
        UserInfo = 1,
        UserAccountInfo,
        UserAccountAndHistoryInfo,
        UserReplenishmentInfo,
        UserNValueCashInfo
    }
}
