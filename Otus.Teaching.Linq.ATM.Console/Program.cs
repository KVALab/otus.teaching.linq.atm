﻿using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;
using System.Collections.Generic;
using System.Linq;

namespace Otus.Teaching.Linq.ATM.Console
{
    class Program
    {
        /// <summary>
        /// Список меню
        /// </summary>
        private static readonly List<string> listMenu = new List<string>()
        {
            "1. Вывод информации о заданном аккаунте по логину и паролю;",
            "2. Вывод данных о всех счетах заданного пользователя;",
            "3. Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту;",
            "4. Вывод данных о всех операциях пополенения счёта с указанием владельца каждого счёта;",
            "5. Вывод данных о всех пользователях у которых на счёте сумма больше N(N задаётся из вне и может быть любой);"
        };

        static ATMManager CreateATMManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();

            return new ATMManager(accounts, users, history);
        }


        static void Main(string[] args)
        {
            System.Console.WriteLine("Старт приложения-банкомата...");

            MenuInit();
            MenuATM(CreateATMManager());

            System.Console.WriteLine("Завершение работы приложения-банкомата...");
        }


        /// <summary>
        /// Вывести меню по операциям
        /// </summary>
        private static void MenuInit()
        {
            listMenu.ForEach(m => System.Console.WriteLine(m));
        }


        /// <summary>
        /// Основной цикл операций банкомата
        /// </summary>
        /// <param name="atmManager">Объект данных</param>
        static void MenuATM(ATMManager atmManager)
        {
            while (true)
            {
                System.Console.WriteLine("\nВведите номер операции или E(для выхода): ");
                var oparation = System.Console.ReadLine();

                //Проверить ввод номера позиции меню
                int mode;
                if (int.TryParse(oparation, out mode) && 
                    (mode >= (int)OperationATM.UserInfo && mode <= (int)OperationATM.UserNValueCashInfo))
                {
                    IEnumerable<Core.Entities.User> loggedUser;
                    switch ((OperationATM)mode)
                    {
                        case OperationATM.UserInfo:
                            if (GetAndCheckUser(atmManager, out loggedUser))
                                GetUserAccountInfo(atmManager, loggedUser);
                            break;
                        case OperationATM.UserAccountInfo:
                            if (GetAndCheckUser(atmManager, out loggedUser))
                                GetUserAccountsInfo(atmManager, loggedUser);
                            break;
                        case OperationATM.UserAccountAndHistoryInfo:
                            if (GetAndCheckUser(atmManager, out loggedUser))
                                GetUserAccountsWithHistoryInfo(atmManager, loggedUser);
                            break;
                        case OperationATM.UserReplenishmentInfo:
                            GetInfoUsersInputCash(atmManager);
                            break;
                        case OperationATM.UserNValueCashInfo:
                            GetInfoUsersNValueCash(atmManager);
                            break;
                    }
                }

                //Выход из программы
                if (!string.IsNullOrEmpty(oparation) &&
                    new List<char>() {'E', 'Е'}.Contains(oparation.ToUpper()[0]))
                    break;
            }
        }


        /// <summary>
        /// Получить данных по пользователям и их аккаунтам по запрашиваемой сумме на счете
        /// </summary>
        /// <param name="atmManager"></param>
        private static void GetInfoUsersNValueCash(ATMManager atmManager)
        {
            System.Console.WriteLine("Введите сумму на счете: ");
            string cash = System.Console.ReadLine();

            //Прворить ввод значения суммы для выбора данных
            int valueCash;
            if (int.TryParse(cash, out valueCash) && valueCash > 0)
            {
                //Проверить превышение суммы в аккаунтах
                if (atmManager.Accounts.Any(c => c.CashAll >= valueCash))
                {
                    //Запрос на вывод пользователей по сумме в аккаунте
                    var userInfoCash = atmManager.Users
                    .GroupJoin(atmManager.Accounts,
                        user => user.Id,
                        account => account.UserId,
                        (user, account) => new
                        {
                            User = user,
                            Accounts = account
                                .Where(a => a.CashAll >= valueCash)
                                .OrderByDescending(o => o.CashAll)
                        })
                    .Where(w => w.Accounts.Any())
                    .OrderBy(u => u.User.Id);

                    //Вывод данных
                    foreach (var item in userInfoCash)
                    {
                        System.Console.WriteLine(item.User);
                        foreach (var account in item.Accounts)
                            System.Console.WriteLine(account);
                    }
                }
                else System.Console.WriteLine("Ошибка: Превышена сумма запроса!");
            }
            else System.Console.WriteLine("Ошибка ввода суммы для запроса!");
        }


        /// <summary>
        /// Вывести данные о польдзователе и его счетах для операции пополнения счета
        /// </summary>
        /// <param name="atmManager"></param>
        private static void GetInfoUsersInputCash(ATMManager atmManager)
        {
            //Запрос
            var historyInputCash = atmManager.Accounts
                .GroupJoin(atmManager.History,
                    account => account.Id,
                    history => history.AccountId,
                    (account, history) => new
                    {
                        User = atmManager.Users.First(u => u.Id == account.UserId),
                        Account = account,
                        History = history
                            .Where(ot => ot.OperationType == Core.Entities.OperationType.InputCash)
                            .OrderByDescending(o => o.OperationDate)
                    })
                .Where(gh => gh.History.Any())
                .GroupBy(gu => gu.User)
                .OrderBy(o => o.Key.Id);

            //Вывести данные
            foreach (var item in historyInputCash)
            {
                System.Console.WriteLine(item.Key);
                foreach (var groups in item)
                {
                    System.Console.WriteLine(groups.Account);
                    foreach (var history in groups.History)
                        System.Console.WriteLine(history);
                }
            }
        }



        /// <summary>
        /// Полная информация по счетам пользователя с историей операций по ним
        /// </summary>
        /// <param name="atmManager"></param>
        /// <param name="loggedUser"></param>
        private static void GetUserAccountsWithHistoryInfo(ATMManager atmManager, IEnumerable<Core.Entities.User> loggedUser)
        {
            var userInfo = GetUserInfo(atmManager, loggedUser);
            var historyAccounts = userInfo
                .First().Value
                .GroupJoin(atmManager.History,
                    account => account.Id,
                    history => history.AccountId,
                    (account, history) => new
                    {
                        Account = account,
                        History = history.Select(h => h).OrderByDescending(o => o.OperationDate)
                    })
                .OrderByDescending(o => o.Account.OpeningDate);

            System.Console.WriteLine($"Пользователь: {loggedUser.First().FirstName} {loggedUser.First().SurName} имеет следующие счета с историей по каждой:");
            foreach (var item in historyAccounts)
            {
                System.Console.WriteLine(item.Account);
                foreach (var history in item.History)
                    System.Console.WriteLine(history);
            }
        }


        /// <summary>
        /// Получить и проверить залогиненого пользователя
        /// </summary>
        /// <param name="atmManager">Объект данных</param>
        /// <param name="loggedUser">Объект найденного залогиненого пользователя</param>
        static bool GetAndCheckUser(ATMManager atmManager, out IEnumerable<Core.Entities.User> loggedUser)
        {
            loggedUser = null;
            int countOperation = 0; //Ввод данных не более 3 раз, иначе выйти

            while (true)
            {
                System.Console.WriteLine("Введите логин пользователя: ");
                string login = System.Console.ReadLine().ToLower();

                System.Console.WriteLine("Введите пароль пользователя: ");
                string password = System.Console.ReadLine().ToLower();

                if (string.IsNullOrEmpty(login) || string.IsNullOrEmpty(password))
                    System.Console.WriteLine($"Пустой логин или пароль недопустимы!");
                else
                {
                    loggedUser = atmManager.Users
                        .Where(user => (user.Login == login && user.Password == password));
                    if (loggedUser.Count() == 0)
                        System.Console.WriteLine($"Пользователь с таким логином: {login} и паролем: {password} не найден!");
                    else return true;
                }

                if (++countOperation >= 3)
                {
                    System.Console.WriteLine($"Исчерпаны все попытки входа пользователя!");
                    return false;
                }
            }
        }


        /// <summary>
        /// Получить конкретный аккаунт польдзователя по его номеру
        /// </summary>
        /// <param name="atmManager"></param>
        /// <param name="loggedUser"></param>
        private static void GetUserAccountInfo(ATMManager atmManager, IEnumerable<Core.Entities.User> loggedUser)
        {
            var userInfo = GetUserInfo(atmManager, loggedUser);

            System.Console.WriteLine($"У пользователя {loggedUser.First().FirstName} {loggedUser.First().SurName} имеется аккаунтов: {userInfo.First().Value.Count()}.");
            if (userInfo.First().Value.Count() == 1)
            {
                System.Console.WriteLine(userInfo.First().Value.First());
            }
            else
            {
                System.Console.WriteLine("Укажите порядковый номер аккаунта: ");
                int numberAccount;
                if (int.TryParse(System.Console.ReadLine(), out numberAccount) &&
                    numberAccount <= userInfo.First().Value.Count() &&
                    numberAccount > 0)
                {
                    var account = userInfo.First().Value.ElementAt(numberAccount - 1);
                    System.Console.WriteLine(account);
                }
            }
        }


        /// <summary>
        /// Получить информацию по всем аккаунтам пользователя
        /// </summary>
        /// <param name="atmManager"></param>
        /// <param name="loggedUser"></param>
        private static void GetUserAccountsInfo(ATMManager atmManager, IEnumerable<Core.Entities.User> loggedUser)
        {
            var userInfo = GetUserInfo(atmManager, loggedUser);

            System.Console.WriteLine($"Пользователь: {loggedUser.First().FirstName} {loggedUser.First().SurName} имеет следующие счета:");
            foreach (var account in userInfo.First().Value)
                System.Console.WriteLine(account);
        }


        /// <summary>
        /// Получить словарь связанных данных по пользователю
        /// </summary>
        /// <param name="atmManager"></param>
        /// <param name="loggedUser"></param>
        /// <returns>Словарь с набором данных</returns>
        static Dictionary<Core.Entities.User, IOrderedEnumerable<Core.Entities.Account>>
            GetUserInfo(ATMManager atmManager, IEnumerable<Core.Entities.User> loggedUser)
        {
            return loggedUser
                .GroupJoin(atmManager.Accounts,
                      user => user.Id,
                      account => account.UserId,
                      (user, accounts) => new Dictionary<Core.Entities.User, IOrderedEnumerable<Core.Entities.Account>>()
                      {
                          {
                              user,
                              accounts.Select(a => a).OrderBy(o => o.Id)
                          }
                      })
                .First();
        }
    }
}