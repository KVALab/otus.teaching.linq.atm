﻿using System;

namespace Otus.Teaching.Linq.ATM.Core.Entities
{
    public class OperationsHistory
    {
        public int Id { get; set; }
        public DateTime OperationDate { get; set; }
        public OperationType OperationType { get; set; }
        public decimal CashSum { get; set; }
        public int AccountId { get; set; }

        public override string ToString()
        {
            string operation = (OperationType == OperationType.InputCash) ? "поступление" : "списание";
            return $"\t\tДата операции: {OperationDate}, Операция-{operation}, Сумма:{CashSum}р.";
        }
    }
}